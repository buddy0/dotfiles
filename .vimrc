set runtimepath+=~/.vim/plugged/

call plug#begin('~/.vim/plugged')
Plug 'morhetz/gruvbox'
Plug 'itchyny/lightline.vim'
Plug 'ap/vim-css-color'
Plug 'arcticicestudio/nord-vim'
Plug 'vimwiki/vimwiki'
Plug 'frazrepo/vim-rainbow'
Plug 'mcchrish/nnn.vim'
"Plug 'puremourning/vimspector'
call plug#end()

"General Settings
syntax on
filetype plugin on
set showcmd
set noshowmode               "Hides INSERT @ bottom of screen
let mapleader=" "
nnoremap <leader>rn :set rnu!<CR>

"Colorscheme Settings
colorscheme gruvbox
set background=dark
set laststatus=2             "Needed to show lightline
let g:lightline = {
    \ 'colorscheme': 'gruvbox'
    \ }

"Groff autocompiling
map <leader>a :w<CR>:! groff -ms %  -T pdf > groff.pdf<CR><CR>
map <leader>z :! zathura groff.pdf & disown<CR><CR>
autocmd BufNewFile,BufRead *.ms set filetype=groff
map <leader>d :! groff -mom % -T pdf > groff.pdf<CR><CR>

"Folds saved for next session
autocmd BufWinLeave * mkview
autocmd BufWinEnter * silent! loadview

"netrw
let g:netrw_winsize = 25
let g:netrw_banner = 0
let g:netrw_browse_split = 3
let g:netrw_liststyle = 3

"vimwiki
set nocompatible

"rainbow
"let g:rainbow_active = 1
au FileType c,cpp,objc,objcpp call rainbow#load()

"nnn
" Start nnn in the current file's directory
nnoremap <leader>n :NnnPicker %:p:h<CR>
let g:nnn#layout = { 'window': { 'width': 0.5, 'height': 0.6, 'highlight': 'Debug' } }
let g:nnn#action = {
      \ '<c-t>': 'tab split',
      \ '<c-x>': 'split',
      \ '<c-v>': 'vsplit' }

"split navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

set splitbelow
set splitright

"compile and debug progras
nnoremap <F4> :vert term<CR>make<CR>
nnoremap <F5> :vert term<CR>gdb output<CR>
tnoremap <Esc> <C-\><C-n>
tnoremap <C-J> <C-W><C-J>
tnoremap <C-K> <C-W><C-K>
tnoremap <C-L> <C-W><C-L>
tnoremap <C-H> <C-W><C-H>

"vimspector
let g:vimspector_enable_mappings = 'HUMAN'
let g:vimspector_install_gadgets = 'vscode-cpptools'

"tabs
set expandtab "replace tabs with spaces
set tabstop=4 "4 spaces for tab key
set shiftwidth=4

