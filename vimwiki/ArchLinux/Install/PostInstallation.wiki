= 64 Bit Library =
* Since I isntalled Arch Linux 64 bit, need to make sure pacman is utlizing the 64 bit library.
* Edit pacman.conf file.
* Uncomment
# vim /etc/pacman.conf
Uncomment #[multilib] & lines below

= Sycronize database =
Syncronize local package database with the remote package database, and update any packages that have any version differences.
# pacman -Syu

= Add user =
* Add user.
* Create password
* Install sudo 
* Add user to appropriate group
* Add user to sudoers file.
* Uncomment
# useradd -m buddy or # useradd -m -g users -s /bin/bash buddy
# passwd buddy
# pacman -S sudo
# usermod -aG wheel,audio,video,optical,storage buddy
# groups buddy
# vim /etc/sudoers
Uncomment # %wheel ALL=(ALL) ALL or/and add buddy ALL=(ALL) ALL

= Automount External Drive =
# lsblk
# sudo mkdir /mnt/seagate
# mount /dev/sdb1 /mnt/seagate
# blkid
# cat /proc/mounts
sudo vim /etc/fstab
UUID=####### /mnt/seagate ext4 rw,relatime 0 0
