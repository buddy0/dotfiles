= Edit mirror list =
# vim /etc/pacman.d/mirrorlist

= Install base package =
# pacstrap -i /mnt base linux linux-firmware

= Fstab (file system table) =
* File to define how disk partitions should be mounted into the fileystem.
* So the distro knows which partitions need to be mounted at boot time.
# genfstab -U /mnt >> /mnt/etc/fstab
# cat /mnt/etc/fstab

= Login into new installation =
# arch-chroot /mnt

= Adjust time zone =
# ln -sf /usr/share/zoneinfo/America/Detroit /etc/localtime

= Synchronize hardware clock with the same clock =
# hwclock --systohc

= Edit locale  file =
* Install vim
* Uncomment.
* Generate.
* Create locale.conf.
* Set LANG var.
# pacman -S vim
# vim /etc/locale.gen
Uncomment # en_US.UTF-8 UTF-8
# locale-gen
# vim /etc/locale.conf
LANG=en_US.UTF-8

= Network Configuration 1st step =
* Create hostname file.
* Naming PC.
* Add matching entries to host
# vim /etc/hostname
arch
# vim /etc/hosts
127.0.0.1 (tab) localhost
::1       (tab) localhost
127.0.0.1 (tab) arch.localdomain (tab) arcb

= Network Configuration 2nd step =
# pacman -S dhcpcd
# systemctl start dhcpcd
# ip a
# systemctl enable dhcpcd@enp0s#.service

= Create Swap File 1st way =
* Naming it swapfile
* Need to change the permission so we can put it into the fstab file
* Make the swap
* Activate the swap.
* Put the swap into fstab (last 0 b/c don't want to check at boot).
# fallocate -l 8GB /swapfile
# chmod 600 /swapfile
# mkswap /swapfile
# swapon /swapfile
# vim /etc/fstab
/swapfile none swap deafaults 0 0

= Create Swap File 2nd way =
* Instead of using fallocate (not sure on the numbers below)
# dd if=/dev/zero of=/swapfile bs=1M count=512 status=progress


= Create password for root =
# passwd

= Mount EFI partition = 
* Want to mount EFI partition into the EFI directory, but it has not yet been created
# mkdir /boot/efi
# mount /dev/sda# /boot/efi
# lsblk

= GRUB =
* Installing some packages
* Install grub at EFI partition.
* Create grub config file
# pacman -S grub efibootmgr
# lsblk
# grub-install --target=x86_64-efi --efi-directoryl=/boot/efi --bootloader-id=GRUB
# grub-mkconfig -o /boot/grub/grub.cfg

= Exit Installation =
* Exit installation and go back to the installe.
* Exits out of our chroot environment and goes back to the root installer.
# exit

= Shutdown =
# shutdown now

!! In VM goto Settings/Storage/ and detach the iso.

