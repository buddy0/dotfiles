= Connect to Internet =
- # ip a
- # ping google.com
- # ^c to kill the command

= Update the system clock =
- # timedatectl set -ntp true
- # timedatectl status

== UEFI w/ GPT | BIOS w/ MBR ==
Guild Partition Table (GPT):: part of the Unified Extensible Firmware Interface
Master Boot Record MBR):: first 512 bytes of storage

= Partitioning =
fdisk:: edits, manipulates partition maps
        need to format partitons w/ appropriate file system
	* UEFI partition: 512 MB    | Type: EFI fielsystem
	* root partition: remainder | Type: linux filesystem (ext4)
- # fdisk /dev/sda
- # g               | creates a new empty GPT partition table label
- # n               | add a new partition
- # +512MB          | EFI partition 
- # t               | change partition type
- # 1               | EFI system
- # n               | added a 2nd partition
- # p               | print the partition table
- # w               | write table to disk and exit

= Formatting =
UEFI partition
- # mkfs.fat -F32 /dev/sda#

root partition
- # mkfs.ext4 /dev/sda#

= Mounting =
Root partition mounting to the mnt directory b/c this is where the system is going to be installed.
- # mount /dev/sda#/ /mnt
