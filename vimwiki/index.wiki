== My Notes ==

== Arch Linux == 
[[ArchLinux/index.wiki|Install]]
[[ArchLinux/Programs/programs.wiki|Programs]]
[[ArchLinux/Programs/gitbarereps.wiki]]
[[ArchLinux/Programs/gitlab.wiki]]
[[ArchLinux/Programs/starship.wiki]]
[[ArchLinux/Programs/suckless_patches.wiki]]
[[ArchLinux/Programs/icons_in_terminal.wiki]]
[[ArchLinux/Programs/xdg.wiki]]
[[ArchLinux/Programs/yay.wiki]]
[[ArchLinux/Programs/vimspector.wiki]]

== Rasperry Pi == 
[[RaspberryPi/index.wiki|Install]]

== Development ==
[[Development/json.wiki|json]]
[[Development/gdb.wiki|gdb]]


