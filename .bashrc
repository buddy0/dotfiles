#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
#git_branch (){
#    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
#}
#export PS1="[\u@\h \W]\$(git_branch)$ "

#set vim as default editior
export VISUAL=vim
export EDITOR=vim

# git bare repo in ~/dotfiles
alias config='/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME'

# starship
eval "$(starship init bash)"

# nnn
export NNN_FCOLORS='c1e2272e006033f7c6d6abc4'
export NNN_COLORS='1234' 
export NNN_PLUG='p:preview-tui;d:dragdrop'
export NNN_FIFO=/tmp/nnn.fifo
alias nnn="nnn -e"

# system
alias ssn="sudo shutdown now"
alias srn="sudo shutdown -r now"


